<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    public function getPages($slug) 
    {
          if (View::exists($slug)) { // check here
            return view($slug);
          }
      return abort(404); // redirect to 404 page
    }
    public function getHome()
    {
        return view('home');
    }
}

// class HomeController extends Controller
// {
//     public function getPages($slug) 
//     {
//         if ($slug)
//         return view()->exists($slug);
//         else
//         abort(404);
//     }
//     public function getHome()
//     {  
//         return view('home');
//     }
// }
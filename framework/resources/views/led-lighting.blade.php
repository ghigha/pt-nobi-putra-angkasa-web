  {{-- header --}}

  @include('../header')

  <main id="main">

    <!-- ======= Blog Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>LED Lighting</h2>

          <ol>
            <li><a href="home">Home</a></li>
            <li><a href="products">Products</a></li>
            <li>LED Lighting</li>
          </ol>
        </div>

      </div>
    </section><!-- End Blog Section -->

    <!-- content -->

    <!-- ======= Features Section ======= -->
    <section class="features">
        <div class="container">
  
          <div class="section-title">
            <h2>LED Lighting</h2>
          </div>
  
          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/Led/led bppt certificte.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Led/LED-BPPT-Certificate.pdf">LED BBPT Test Certificate</a></h4>
              <p class="font-italic">
                We intend to be the market leader in South East Asia Cable Tray and Enclosures Fabricator.
              </p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
              </ul> -->
            </div>
          </div>
  
          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/Led/led tube lamp.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Led/LED-Tube-Lamp.pdf">LED Tube Lamp</a></h4>
              <!-- <p class="font-italic">
                To deliver high quality products , to meet the requirements of our clients and the needs of the market.
              </p> -->
              <p>
                To deliver high quality products , to meet the requirements of our clients and the needs of the market.
              </p>
            </div>
          </div>
  
          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/Led/led street lamp.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Led/LED-Street-Lamp.pdf">LED Street Lamp</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/Led/nl500.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Led/nl500.pdf">NL500 Stadium Lighting</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>
  
        </div>
      </section><!-- End Features Section -->

    <!-- End content -->


  </main><!-- End #main -->

  {{-- footer --}}
  @include('../footer')
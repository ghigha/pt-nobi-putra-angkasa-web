  {{-- header --}}

  @include('../header')

  <main id="main">

    <!-- ======= Blog Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>SNI Certificate</h2>

          <ol>
            <li><a href="home">Home</a></li>
            <li><a href="certification">Certification</a></li>
            <li>SNI Certificate</li>
          </ol>
        </div>

      </div>
    </section><!-- End Blog Section -->

    <!-- content -->
 
        <div style="text-align: center;"><iframe src="assets/files/SNI_ISO_9001-2015.pdf" width="1100px" height="2400px" frameborder="0"></iframe></div>

    <!-- End content -->

  </main><!-- End #main -->

{{-- footer --}}
@include('../footer')
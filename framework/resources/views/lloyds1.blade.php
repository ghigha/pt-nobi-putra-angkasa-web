  {{-- header --}}

  @include('../header')

  <main id="main">

    <!-- ======= Blog Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>LLoyd's Certificate LRQA Cable Tray</h2>

          <ol>
            <li><a href="home">Home</a></li>
            <li><a href="certification">Certification</a></li>
            <li>LLoyd's Certificate LRQA Cable Tray</li>
          </ol>
        </div>

      </div>
    </section><!-- End Blog Section -->

    <!-- content -->
 
        <div style="text-align: center;"><iframe src="assets/files/llyod" width="1200px" height="6000px" frameborder="0"></iframe></div>

    <!-- End content -->

  </main><!-- End #main -->

 {{-- footer --}}
 @include('../footer')
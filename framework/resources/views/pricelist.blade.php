  {{-- header --}}

  @include('../header')

  <main id="main">

    <!-- ======= Our Team Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Price List Speed Radar</h2>
          <ol>
            <li><a href="home">Home</a></li>
            <li>Price List Speed Radar</li>
          </ol>
        </div>

      </div>
    </section><!-- End Our Team Section -->

    <!-- ======= Why Us Section ======= -->
    <section class="why-us section-bg" data-aos="fade-up" date-aos-delay="200">
      <div class="container">

        <div class="section-title">
          <h2>Price List Speed Radar</h2>
        </div>

        <div class="row">

          <!--<div class="container">-->
            <!--<div class="card mt-5">-->
              <div class="card-body">
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th width="5%">No.</th>
                      <th>Item</th>
                      <th>Image</th>
                      <th>Price</th>
                    </tr>
                  </thead>
                  <tbody> 
                  <tr> 
                    <th>1</th>
                    <th><a href="assets/img/Price_List/Speed Radar.jpg" target="_blank">Pole Portable</a></th>
                    <!--<th> <img src="assets/img/Price_List/Speed Radar.jpg" href="assets/img/Price_List/Speed Radar.jpg" width="300" height="200" alt="Pole Portable" /> </th>-->
                    <th> <img id="myImg" src="assets/img/Price_List/Speed Radar.jpg" alt="Pole Portable" style="width:100%;max-width:500px"> </th>
                    <th>Rp 56,000,000</th> 
                    </tr>
                     <tr>    
                    <th>2</th>
                    <th><a href="assets/img/Price_List/Box, Battery dan Solar Cell.jpg" target="_blank">Box, Battery dan Solar Cell</a></th>
                    <!--<th> <img src="assets/img/Price_List/Box, Battery dan Solar Cell.jpg" width="300" height="200" alt="Pole Portable" /> </th>-->
                    <th> <img id="myImg1" src="assets/img/Price_List/Box, Battery dan Solar Cell.jpg" alt="Box, Battery dan Solar Cell" style="width:100%;max-width:500px"> </th>
                    <th>Rp 123,000,000</th> 
                     </tr>
                    <tr> 
                    <th>3</th>
                    <th><a href="assets/img/Price_List/Speed%20Radar%20(Single%20Standing).jpg" target="_blank">Single Standing</a></th>
                    <!--<th> <img src="assets/img/Price_List/Speed%20Radar%20(Single%20Standing).jpg" width="300" height="200" alt="Pole Portable" /> </th>-->
                    <th> <img id="myImg2" src="assets/img/Price_List/Speed%20Radar%20(Single%20Standing).jpg" alt="Single Standing" style="width:100%;max-width:500px"> </th>
                    <th>Rp 35,000,000</th> 
                    </tr>
                     <tr> 
                    <th>4</th>
                    <th><a href="assets/img/Price_List/Speed%20Radar%20(1%20Lane).jpg" target="_blank">1 Lane</a></th>
                    <!--<th> <img src="assets/img/Price_List/Speed%20Radar%20(1%20Lane).jpg" width="300" height="200" alt="Pole Portable" /> </th>-->
                    <th> <img id="myImg3" src="assets/img/Price_List/Speed%20Radar%20(1%20Lane).jpg" alt="1 Lane" style="width:100%;max-width:500px"> </th>
                    <th>Rp 176,000,000</th> 
                    </tr>
                     <tr> 
                    <th>5</th>
                    <th><a href="assets/img/Price_List/Speed%20Radar%20(2%20Lane).jpg" target="_blank">2 Lane</a></th>
                    <!--<th> <img src="assets/img/Price_List/Speed%20Radar%20(2%20Lane).jpg" width="300" height="200" alt="Pole Portable" /> </th>-->
                    <th> <img id="myImg4" src="assets/img/Price_List/Speed%20Radar%20(2%20Lane).jpg" alt="2 Lane" style="width:100%;max-width:500px"> </th>
                    <th>Rp 232,000,000</th> 
                    </tr>
                     <tr> 
                    <th>6</th>
                    <th><a href="assets/img/Price_List/Speed%20Radar%202%20Lane%20(Left%20&%20Right).jpg" target="_blank">2 Lane (left & right)</a></th>
                    <!--<th> <img src="assets/img/Price_List/Speed%20Radar%202%20Lane%20(Left%20&%20Right).jpg" width="300" height="200" alt="Pole Portable" /> </th>-->
                    <th> <img id="myImg5" src="assets/img/Price_List/Speed%20Radar%202%20Lane%20(Left%20&%20Right).jpg" alt="2 Lane (left & right)" style="width:100%;max-width:500px"> </th>
                    <th>Rp 239,000,000</th> 
                    </tr>
                    <tr> 
                    <th>7</th>
                    <th><a href="assets/img/Price_List/Speed%20Radar%204%20Lane%20(2Left%20&%202Right).jpg" target="_blank">4 Lane (2 left & 2 right)</a></th>
                    <!--<th> <img src="assets/img/Price_List/Speed%20Radar%204%20Lane%20(2Left%20&%202Right).jpg" width="300" height="200" alt="Pole Portable" /> </th>-->
                    <th> <img id="myImg6" src="assets/img/Price_List/Speed%20Radar%204%20Lane%20(2Left%20&%202Right).jpg" alt="4 Lane (2 left & 2 right)" style="width:100%;max-width:500px"> </th>
                    <th>Rp 331,000,000</th> 
                    </tr>
                    <tr> 
                    <th>8</th>
                    <th><a href="assets/img/Price_List/Portable%20Genset.jpg" target="_blank">Genset 800W</a></th>
                    <!--<th> <img src="assets/img/Price_List/Portable%20Genset.jpg" width="300" height="200" alt="Pole Portable" /> </th>-->
                    <th> <img id="myImg7" src="assets/img/Price_List/Portable%20Genset.jpg" alt="Genset 800W" style="width:100%;max-width:500px"> </th>
                    <th>Rp 51,000,000</th> 
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          <!--</div>-->
          <!--</div>-->

<!-- The Modal -->
<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>

<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}

// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg1");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}

// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg2");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}

// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg3");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}

// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg4");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}

// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg5");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}

// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg6");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}

// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg7");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>

          <!-- <div class="col-lg-6 d-flex flex-column justify-content-center p-5">

            <div class="icon-box">
              <div class="icon"><i class="bx bx-fingerprint"></i></div>
              <h4 class="title"><a href="">LED Light</a></h4>
              <p class="description">SAMPLE</p>
            </div>

            <div class="icon-box">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">LED Neon</a></h4>
              <p class="description">SAMPLE</p>
            </div>

          </div> -->
        </div>


    </section><!-- End Why Us Section -->

    <!-- <div class="col-lg-3 col-md-6 footer-info">
      <h3>About PT. Nobi Putra Angkasa</h3>
      <p>Appointed as Siemens System Integrator in LV and ABB TCA in MV Solution</p>
      <div class="social-links mt-3">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="whatsapp"><i class="bx bxl-whatsapp"></i></a>
      </div>
    </div> -->

  </main><!-- End #main -->

 {{-- footer --}}
 @include('../footer')
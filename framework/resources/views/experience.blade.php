  {{-- header --}}

  @include('../header')

  <main id="main">

    <!-- ======= Our Team Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Experience</h2>
          <ol>
            <li><a href="home">Home</a></li>
            <li>Experience</li>
          </ol>
        </div>

      </div>
    </section><!-- End Our Team Section -->

    <!-- ======= Features Section ======= -->
    <section class="features">
      <div class="container">

        <div class="section-title">
          <h2>Experience</h2>
        </div>

        <div class="row">

          <div class="container">
            <div class="card mt-5">
              <div class="card-body">
                <h3 class="text-center"><a>OIL, GAS & MINING INDUSTRY PROJECTS</a></h3>
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th width="1%">No.</th>
                      <th>Project</th>
                      <th>Client</th>
                      <th>Year</th>
                      <th>Scope</th>
                      <th>Material</th>
                    </tr>
                  </thead>
                  <tbody>                                     
                    <th>1</th>
                    <th>Asamera Platform</th>
                    <th>Asamera Oil</th>
                    <th>1986</th>  
                    <th>Cable Ladder</th>
                    <th>SS316L</th>
                    <tr>     
                    <th>2</th>
                    <th>LNG Arunm</th>
                    <th>LNG Arun</th>
                    <th>1988</th>  
                    <th>Cable Ladder</th>
                    <th>Aluminium</th>
                    </tr>  
                    <th>3</th>
                    <th>BP</th>
                    <th>PT. Erka Paksi Andini</th>
                    <th>2008</th>  
                    <th>Cable Ladder</th>
                    <th>Aluminum</th>
                    </tr> 
                    <th>4</th>
                    <th>Balikpapan Minor Work</th>
                    <th>PT. ODG Wormald</th>
                    <th>2008</th>  
                    <th>Cable Ladder</th>
                    <th>HDG Steel</th>
                    </tr> 
                    <th>5</th>
                    <th>Bekapai Platform</th>
                    <th>PT. Nippon Steel</th>
                    <th>2008</th>  
                    <th>Cable Tray</th>
                    <th>SS316L</th> 
                    </tr> 
                    <th>6</th>
                    <th>SINGA GAS</th>
                    <th>PT. INTI KARYA PERSADA</th>
                    <th>2008</th>  
                    <th>Cable Ladder</th>
                    <th>HDG Steel</th>
                    </tr> 
                    <th>7</th>
                    <th>Suban Gas</th>
                    <th>PT. SEMPEC INDONESIA</th>
                    <th>2009</th>  
                    <th>Cable Ladder</th>
                    <th>Aluminum</th>
                    </tr> 
                    <th>8</th>
                    <th>Semarang LPG Facilities</th>
                    <th>PT. CITRA PANJI MANUNGGAL</th>
                    <th>2009</th>  
                    <th>Cable Ladder</th>
                    <th>Aluminum</th>
                    </tr> 
                    <th>9</th>
                    <th>NEWMONT</th>
                    <th>NEWMONT NUSA TENGGARA</th>
                    <th>2009</th>  
                    <th>Cable Ladder</th>
                    <th>SS316L</th>
                    </tr>  
                    <th>10</th>
                    <th>BP</th>
                    <th>INDOKOMAS</th>
                    <th>2009</th>  
                    <th>Cable Tray</th> 
                    <th>Aluminium</th>
                    </tr>    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
            
          <!-- <div class="col-lg-6 video-box">
            <img src="assets/img/logo/led.jpg" class="img-fluid" alt="">
            <a href="https://www.youtube.com/" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a> -->
          </div>

          <div class="row">

            <div class="container">
              <div class="card mt-5">
                <div class="card-body">
                  <h3 class="text-center"><a>POWER PLANT PROJECTS</a></h3>
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th width="1%">No.</th>
                        <th>Project</th>
                        <th>Client</th>
                        <th>Year</th>
                        <th>Scope</th>
                        <th>Material</th>
                      </tr>
                    </thead>
                    <tbody>                                     
                      <th>1</th>
                      <th>PLTU Cilacap</th>
                      <th>Sumber Segara</th>
                      <th>2009</th>  
                      <th>Cable Ladder</th>
                      <th>Aluminum</th>
                      <tr>     
                      <th>2</th>
                      <th>Nga Awa Pura NEW ZEALAND</th>
                      <th>TOKYO ENERGY</th>
                      <th>2008</th>  
                      <th>Cable Ladder</th>
                      <th>Aluminium</th>
                      </tr>  
                      <th>3</th>
                      <th>Cikarang Listrindo</th>
                      <th>PT. Siemens Indonesia</th>
                      <th>2008</th>  
                      <th>Cable Tray</th>
                      <th>HDG Steel</th>
                      </tr> 
                      <th>4</th>
                      <th>PLTU REMBANG</th>
                      <th>JO ZELAN PRIAMANAYA</th>
                      <th>2008</th>  
                      <th>Cable Ladder</th>
                      <th>HDG Steel</th>
                      </tr> 
                      <th>5</th>
                      <th>Wayang Windu Phase 2 Geothermal Power</th>
                      <th>Moritani Singapore Pte Ltd</th>
                      <th>2008</th>  
                      <th>Cable Tray + Accessories</th>
                      <th>Aluminium</th> 
                      </tr> 
                      <th>6</th>
                      <th>PLTU Muara Karang GPP 2 Switchyards &2 Substations</th>
                      <th>Sumitomo Corporation</th>
                      <th>2008</th>  
                      <th>Support + Acessories</th>
                      <th>HDG Steel</th>
                      </tr> 
                      <th>7</th>
                      <th>Lahendong III Geothermal Power Plant</th>
                      <th>PT. Rekayasa Industri</th>
                      <th>2008</th>  
                      <th>Cable Tray</th>
                      <th>HDG Steel</th>
                      </tr> 
                      <th>8</th>
                      <th>Kawerau Geothermal Power Project (New Zealand), 1x90 MW</th>
                      <th>Fuji Electric System Co., Ltd</th>
                      <th>2008</th>  
                      <th>Cable Tray +acc</th>
                      <th>Aluminum</th>
                      </tr> 
                      <th>9</th>
                      <th>PLTU Tarahan</th>
                      <th>CV. Starlite Outdoor</th>
                      <th>2007</th>  
                      <th>Cable Ladder</th>
                      <th>HDG Steel</th>
                      </tr>  
                      <th>10</th>
                      <th>Tanjung Jati B Power Plant</th>
                      <th>Black & Veatch Int'l Ltd</th>
                      <th>2005</th>  
                      <th>N1000</th> 
                      <th>HDG Steel</th>
                      </tr>    
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
              
            <!-- <div class="col-lg-6 video-box">
              <img src="assets/img/logo/led.jpg" class="img-fluid" alt="">
              <a href="https://www.youtube.com/" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a> -->
            </div>

            <div class="row">

              <div class="container">
                <div class="card mt-5">
                  <div class="card-body">
                    <h3 class="text-center"><a>COMMERCIAL, GOVERNMENT AND OFFICE BUILDING PROJECTS</a></h3>
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th width="1%">No.</th>
                          <th>Project</th>
                          <th>Client</th>
                          <th>Year</th>
                          <th>Scope</th>
                          <th>Material</th>
                        </tr>
                      </thead>
                      <tbody>                                     
                        <th>1</th>
                        <th>Pauwels Trafo Asia</th>
                        <th>PT. Pauwels Trafo Asia</th>
                        <th>2009</th>
                        <th>Cable Tray</th>
                        <th>HDG Steel</th>
                        <tr>     
                        <th>2</th>
                        <th>PARIT PADANG</th>
                        <th>Giniputra Kharisma</th>
                        <th>2008</th>
                        <th>Cable Ladder</th>
                        <th>HDG Steel</th>
                        </tr>  
                        <th>3</th>
                        <th>PERURI Kerawang</th>
                        <th>PT. Indomuda Satria</th>
                        <th>2007</th>
                        <th>Cable Ladder</th>
                        <th>HDG Steel</th>
                        </tr> 
                        <th>4</th>
                        <th>Guido Valarades Hospital Dili</th>
                        <th>Penta Ocean Constr</th>
                        <th>2007</th>
                        <th>Cable Ladder</th>
                        <th>HDG Steel</th>
                        </tr> 
                        <th>5</th>
                        <th>Landmark PJT</th>
                        <th>PT. Agung Eltramas</th>
                        <th>2002</th>
                        <th>Cable Tray</th>
                        <th>HDG Steel</th>
                        </tr> 
                        <th>6</th>
                        <th>Fishing Port Muara Baru</th>
                        <th>PT. Kontindo Panca M</th>
                        <th>2002</th>
                        <th>Cable Ladder</th>
                        <th>HDG Steel</th>
                        </tr> 
                        <th>7</th>
                        <th>Apartemen Pantai Mutiara</th>
                        <th>PT. Stella Satindo</th>
                        <th>2002</th>
                        <th>Cable Ladder</th>
                        <th>HDG Steel</th>
                        </tr> 
                        <th>8</th>
                        <th>Apartemen Pantai Mutiara</th>
                        <th>PT. Arga Prasetya</th>
                        <th>2002</th>
                        <th>Cable Ladder</th>
                        <th>HDG Steel</th>
                        </tr> 
                        <th>9</th>
                        <th>Makro - Medan</th>
                        <th>PT. ODG Wormald</th>
                        <th>2000</th>
                        <th>Cable Tray</th>
                        <th>HDG Steel</th>
                        </tr>  
                        <th>10</th>
                        <th>Plaza Citra - Pekanbaru</th>
                        <th>PT. Buana Pendawa</th>
                        <th>1999</th>
                        <th>Cable Tray</th>
                        <th>HDG Steel</th>
                        </tr>    
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
                
              <!-- <div class="col-lg-6 video-box">
                <img src="assets/img/logo/led.jpg" class="img-fluid" alt="">
                <a href="https://www.youtube.com/" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a> -->
              </div>

              <div class="row">

                <div class="container">
                  <div class="card mt-5">
                    <div class="card-body">
                      <h3 class="text-center"><a>FACTORY AND INDUSTRY BUILDING PROJECTS</a></h3>
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th width="1%">No.</th>
                            <th>Project</th>
                            <th>Client</th>
                            <th>Year</th>
                            <th>Scope</th>
                            <th>Material</th>
                          </tr>
                        </thead>
                        <tbody>                                     
                          <th>1</th>
                          <th>Siemens Cilegon Factory</th>
                          <th>PT. Siemens Indonesia</th>
                          <th>2009</th>
                          <th>Cable Tray</th>
                          <th>HDG Steel</th>
                          <tr>     
                          <th>2</th>
                          <th>Semen Padang</th>
                          <th>CV. Putra Medan Suri</th>
                          <th>2009</th>
                          <th>Cable Tray</th>
                          <th>HDG Steel</th>
                          </tr>  
                          <th>3</th>
                          <th>Proyek Instalasi, Mixing Campina</th>
                          <th>PT. Campina</th>
                          <th>2008</th>
                          <th>Cable Tray</th>
                          <th>HDG Steel</th>
                          </tr> 
                          <th>4</th>
                          <th>P0514-SOHO</th>
                          <th>PT. Arista</th>
                          <th>2008</th>
                          <th>Cable Tray</th>
                          <th>HDG Steel</th>
                          </tr> 
                          <th>5</th>
                          <th>INCO Soroako</th>
                          <th>PT. International Nickel Ind</th>
                          <th>2008</th>
                          <th>Cable Ladder</th>
                          <th>SS316</th>
                          </tr> 
                          <th>6</th>
                          <th>INCO Soroako</th>
                          <th>PT. International Nickel Ind</th>
                          <th>2007</th>
                          <th>Cable Ladder</th>
                          <th>HDG Steel</th>
                          </tr> 
                          <th>7</th>
                          <th>SOHO Industri Farmasi</th>
                          <th>PT. Giniputra Kharisma</th>
                          <th>2007-2008</th>
                          <th>Cable Tray</th>
                          <th>HDG Steel</th>
                          </tr> 
                          <th>8</th>
                          <th>INCO Soroako</th>
                          <th>PT. International Nickel Ind</th>
                          <th>2004</th>
                          <th>Cable Ladder</th>
                          <th>HDG Steel</th>
                          </tr> 
                          <th>9</th>
                          <th>Gudang Garam W/H</th>
                          <th>PT. Arista Pratama Jaya</th>
                          <th>2004</th>
                          <th>Cable Tray</th>
                          <th>HDG Steel</th>
                          </tr>  
                          <th>10</th>
                          <th>Pupuk Iskandar Muda 2 Aceh</th>
                          <th>PT. Multi Karya Develindo</th>
                          <th>2003</th>
                          <th>Cable Ladder</th>
                          <th>HDG Steel</th>
                          </tr>    
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                  
                <!-- <div class="col-lg-6 video-box">
                  <img src="assets/img/logo/led.jpg" class="img-fluid" alt="">
                  <a href="https://www.youtube.com/" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a> -->
                </div>

                <div class="row">

                  <div class="container">
                    <div class="card mt-5">
                      <div class="card-body">
                        <h3 class="text-center"><a>TELECOMMUNICATION PROJECTS</a></h3>
                        <table class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th width="1%">No.</th>
                              <th>Project</th>
                              <th>Client</th>
                              <th>Year</th>
                              <th>Scope</th>
                              <th>Material</th>
                            </tr>
                          </thead>
                          <tbody>                                     
                            <th>1</th>
                            <th>Site Gayungan + Gombel</th>
                            <th>PT. TELKOMSEL</th>
                            <th>2009</th>
                            <th>DDF K-52 Rac</th>
                            <th></th>
                            <tr>     
                            <th>2</th>
                            <th>TTC Pematang Siantar</th>
                            <th>PT. Agung Mas</th>
                            <th>2009</th>
                            <th>DDF K-52 Rack</th>
                            <th></th>
                            </tr>  
                            <th>3</th>
                            <th>NSN</th>
                            <th>PT. Suryajaya Teknotama</th>
                            <th>2009</th>
                            <th>DDF K-52 Rack</th>
                            <th></th>
                            </tr> 
                            <th>4</th>
                            <th>INDOSAT 3G 2009</th>
                            <th>PT. Dwi Pilar</th>
                            <th>2009</th>
                            <th>ACPDB</th>
                            <th></th>
                            </tr> 
                            <th>5</th>
                            <th>TELKOMSEL 3G</th>
                            <th>PT. Ericsson Indonesia</th>
                            <th>2009</th>
                            <th>DDF K-52 Rack</th>
                            <th></th>
                            </tr> 
                            <th>6</th>
                            <th>Sarpen Medan Rantau</th>
                            <th>PT. Agung Mas</th>
                            <th>2009</th>
                            <th>DDF K-52 Rack</th>
                            <th></th>
                            </tr> 
                            <th>7</th>
                            <th>EXCELCOM</th>
                            <th>PT. Ericsson Indonesia</th>
                            <th>2009</th>
                            <th>NOE RBS XL</th>
                            <th></th>
                            </tr> 
                            <th>8</th>
                            <th>EXCELCOM QTC</th>
                            <th>PT. Ericsson Indonesia</th>
                            <th>2009</th>
                            <th>Minilink</th>
                            <th></th>
                            </tr> 
                            <th>9</th>
                            <th>TELKOM FLEXI</th>
                            <th>PT. Indo Multi Daya</th>
                            <th>2009</th>
                            <th>KWH,MDP</th>
                            <th></th>
                            </tr>  
                            <th>10</th>
                            <th>NOKIA HCPT</th>
                            <th>PT. NEXWAVE</th>
                            <th>2009</th>
                            <th>Cab BS241</th>
                            <th></th>
                            </tr>    
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                    
                  <!-- <div class="col-lg-6 video-box">
                    <img src="assets/img/logo/led.jpg" class="img-fluid" alt="">
                    <a href="https://www.youtube.com/" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a> -->
                  </div>

                  <div class="row">

                    <div class="container">
                      <div class="card mt-5">
                        <div class="card-body">
                          <h3 class="text-center"><a>OTHER PROJECTS</a></h3>
                          <table class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th width="1%">No.</th>
                                <th>Project</th>
                                <th>Client</th>
                                <th>Year</th>
                                <th>Scope</th>
                                <th>Material</th>
                              </tr>
                            </thead>
                            <tbody>                                     
                              <th>1</th>
                              <th>Dbl Track Pemalang-Larangan</th>
                              <th>PT. LEN Industri</th>
                              <th>2008</th>
                              <th>Free Standing Enclosure</th>
                              <th></th>
                              <tr>     
                              <th>2</th>
                              <th>Persinyalan&Telekomunikasi Sumut</th>
                              <th>PT. LEN Industri</th>
                              <th>2008</th>
                              <th>Free Standing Enclosure</th>
                              <th></th>
                              </tr>  
                              <th>3</th>
                              <th>Sinyal elektrik Prabumulih</th>
                              <th>PT. LEN Industri</th>
                              <th>2008</th>
                              <th>Free Standing Enclosure</th>
                              <th></th>
                              </tr> 
                              <th>4</th>
                              <th>Persinyalan Serpong-Parung Panjang</th>
                              <th>PT. LEN Industri</th>
                              <th>2008</th>
                              <th>Free Standing Enclosure</th>
                              <th></th>
                              </tr> 
                              <th>5</th>
                              <th>Modifikasi sinyal Purwokerto- Patuguran</th>
                              <th>PT. LEN Industri</th>
                              <th>2008</th>
                              <th>Free Standing Enclosure</th>
                              <th></th>
                              </tr> 
                              <th>6</th>
                              <th>SIL 02 Bangil Tahap II</th>
                              <th>PT. LEN Industri</th>
                              <th>2008</th>
                              <th>Marshaling Enclosure</th>
                              <th></th>
                              </tr> 
                              <th>7</th>
                              <th>Basement 1 Arkadia Building</th>
                              <th>PT.Siemens Indonesia</th>
                              <th>2008</th>
                              <th>Wall Mounting Enclosure</th>
                              <th></th>
                              </tr> 
                              <th>8</th>
                              <th>Freeport Fire suppression System</th>
                              <th>PT. ODG Wormald</th>
                              <th>2008</th>
                              <th>Wall Mounting Enclosure</th>
                              <th></th>
                              </tr> 
                              <th>9</th>
                              <th>YTI/USA Container Crane Works</th>
                              <th>ACE Service Co,.Ltd</th>
                              <th>2008</th>
                              <th>Wall Mounting Enclosure</th>
                              <th></th>
                              </tr>  
                              <th>10</th>
                              <th>Rangau Utility Water</th>
                              <th>WahanaKarsa</th>
                              <th>2009</th>
                              <th>Wall Mounting Enclosure</th>
                              <th></th>
                              </tr>   
                              <th>11</th>
                              <th>Persinyalan + Telekomunikasi Sumut</th>
                              <th>LEN Industri</th>
                              <th>2009</th>
                              <th>Free Standing Enclosure</th>
                              <th></th> 
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                      
                    <!-- <div class="col-lg-6 video-box">
                      <img src="assets/img/logo/led.jpg" class="img-fluid" alt="">
                      <a href="https://www.youtube.com/" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a> -->
                    </div>

                    <div class="row">

                      <div class="container">
                        <div class="card mt-5">
                          <div class="card-body">
                            <h3 class="text-center"><a>POWER PLANT PROJECTS (ENCLOSURE)</a></h3>
                            <table class="table table-bordered table-striped">
                              <thead>
                                <tr>
                                  <th width="1%">No.</th>
                                  <th>Project</th>
                                  <th>Client</th>
                                  <th>Year</th>
                                  <th>Scope</th>
                                  <th>Material</th>
                                </tr>
                              </thead>
                              <tbody>                                     
                                <th>1</th>
                                <th>PLN Jatirangon</th>
                                <th>PT. Siemens Indonesia</th>
                                <th>2004</th>
                                <th>Free Standing SS3000</th>
                                <th></th>
                                <tr>     
                                <th>2</th>
                                <th>Pluak Daeng Power Plant</th>
                                <th>PT. Siemens Indonesia</th>
                                <th>2005</th>
                                <th>Free Standing SS3000</th>
                                <th></th>
                                </tr>  
                                <th>3</th>
                                <th>PLN Ciamis</th>
                                <th>PT. Siemens Indonesia</th>
                                <th>2005</th>
                                <th>Marshaling Kios</th>
                                <th></th>
                                </tr> 
                                <th>4</th>
                                <th>Ha Tinh Power Plant</th>
                                <th>PT. Siemens Indonesia</th>
                                <th>2005</th>
                                <th>Free Standing SS3000</th>
                                <th></th>
                                </tr> 
                                <th>5</th>
                                <th>PLN Karawang Baru</th>
                                <th>PT. Siemens Indonesia</th>
                                <th>2005</th>
                                <th>Free Standing SS3000</th>
                                <th></th>
                                </tr> 
                                <th>6</th>
                                <th>Add. Dong Ha + Muara Karang</th>
                                <th>PT. SIEMENS</th>
                                <th>2009</th>
                                <th>Free Standing SS3000</th>
                                <th></th>
                                </tr> 
                                <th>7</th>
                                <th>Middletown 66MVA, USA</th>
                                <th>PT. Unelec Indonesia</th>
                                <th>2009</th>
                                <th>Wall Mounting</th>
                                <th></th>
                                </tr> 
                                <th>8</th>
                                <th>Benin</th>
                                <th>PT. Unelec Indonesia</th>
                                <th>2009</th>
                                <th>Wall Mounting</th>
                                <th></th>
                                </tr> 
                                <th>9</th>
                                <th>GI 150kV Maros</th>
                                <th>PT. Siemens Indonesia</th>
                                <th>2009</th>
                                <th>Marshaling Kiosk</th>
                                <th></th>
                                </tr>  
                                <th>10</th>
                                <th>SURALAYA</th>
                                <th>PT. GUNA ELEKTRO</th>
                                <th>2009</th>
                                <th>Wall Mounting</th>
                                <th></th>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                        
                      <!-- <div class="col-lg-6 video-box">
                        <img src="assets/img/logo/led.jpg" class="img-fluid" alt="">
                        <a href="https://www.youtube.com/" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a> -->
                      </div>

                      <div class="row">

                        <div class="container">
                          <div class="card mt-5">
                            <div class="card-body">
                              <h3 class="text-center"><a>Factory Projects (Enclosure)</a></h3>
                              <table class="table table-bordered table-striped">
                                <thead>
                                  <tr>
                                    <th width="1%">No.</th>
                                    <th>Project</th>
                                    <th>Client</th>
                                    <th>Year</th>
                                    <th>Scope</th>
                                    <th>Material</th>
                                  </tr>
                                </thead>
                                <tbody>                                     
                                  <th>1</th>
                                  <th>PAL-SANTOS</th>
                                  <th>PT. Yokogawa</th>
                                  <th>2009</th>
                                  <th>Free Standing SS3000</th>
                                  <th>Powder Coating Steel</th>
                                  <tr>     
                                  <th>2</th>
                                  <th>PHILLIPS</th>
                                  <th>PT. SIEMENS INDONESIA</th>
                                  <th>2008</th>
                                  <th>Free Standing SS3000</th>
                                  <th>Powder Coating Steel</th>
                                  <th></th>
                                  </tr>  
                                  <th>3</th>
                                  <th>SANTOS ASIA PASIFIC</th>
                                  <th>PT. YOKOGAWA/PAL</th>
                                  <th>2008</th>
                                  <th>Free Standing SS3000</th>
                                  <th>Powder Coating Steel</th>
                                  </tr> 
                                  <th>4</th>
                                  <th>TOTAL</th>
                                  <th>PT. Dian Citra Utama</th>
                                  <th>2007</th>
                                  <th>Free Standing SS3000</th>
                                  <th>Powder Coating Steel</th>
                                  </tr> 
                                  <th>5</th>
                                  <th>Bosowa</th>
                                  <th>PT. Siemens Indonesia</th>
                                  <th>2007</th>
                                  <th>Free Standing SS3000</th>
                                  <th>Powder Coating Steel</th>
                                  </tr> 
                                  <th>6</th>
                                  <th>Trafo Industry</th>
                                  <th>PT. Pauwels Trafo Asia</th>
                                  <th>2005</th>
                                  <th>Cubicle Enclosures</th>
                                  <th>Powder Coating Steel</th>
                                  </tr> 
                                  <th>7</th>
                                  <th>Sampoerna Surabaya</th>
                                  <th>PT. Gunung Anyar</th>
                                  <th>2005</th>
                                  <th>Rack U + SS3000</th>
                                  <th>Powder Coating Steel</th>
                                  </tr> 
                                  <th>8</th>
                                  <th>Paper Industry</th>
                                  <th>PT. Ciptapaperia</th>
                                  <th>2005</th>
                                  <th>Free Standing SS3000</th>
                                  <th>Powder Coating Steel</th>
                                  </tr> 
                                  <th>9</th>
                                  <th>Kawasaki Industry</th>
                                  <th>PT. Siemens Indonesia</th>
                                  <th>2005</th>
                                  <th>Free Standing SS3000</th>
                                  <th>Powder Coating Steel</th>
                                  </tr>  
                                  <th>10</th>
                                  <th>Tuban Aromatic</th>
                                  <th>PT. Control Systems</th>
                                  <th>2005</th>
                                  <th>Free Standing SS3000</th>
                                  <th>Powder Coating Steel</th>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                          
                        <!-- <div class="col-lg-6 video-box">
                          <img src="assets/img/logo/led.jpg" class="img-fluid" alt="">
                          <a href="https://www.youtube.com/" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a> -->
                        </div>

                        <div class="row">

                          <div class="container">
                            <div class="card mt-5">
                              <div class="card-body">
                                <h3 class="text-center"><a>OIL& GAS PROJECTS (ENCLOSURE)</a></h3>
                                <table class="table table-bordered table-striped">
                                  <thead>
                                    <tr>
                                      <th width="1%">No.</th>
                                      <th>Project</th>
                                      <th>Client</th>
                                      <th>Year</th>
                                      <th>Scope</th>
                                      <th>Material</th>
                                    </tr>
                                  </thead>
                                  <tbody>                                     
                                    <th>1</th>
                                    <th>SINGA GAS</th>
                                    <th>PT. Inti Karya Persada Teknik</th>
                                    <th>2009</th>
                                    <th>GLAND BOX</th>
                                    <th>Steel HDG</th>
                                    <tr>     
                                    <th>2</th>
                                    <th>Chevron</th>
                                    <th>PT. Gas Securicor</th>
                                    <th>2009</th>
                                    <th>Wall Mounted NCE</th>
                                    <th>Steel PC</th>
                                    </tr>  
                                    <th>3</th>
                                    <th>MEDCO PLC Load</th>
                                    <th>PT. Promatcon Tepatguna</th>
                                    <th>2009</th>
                                    <th>Free standing</th>
                                    <th>Steel PC</th>
                                    </tr> 
                                    <th>4</th>
                                    <th>Petrochina</th>
                                    <th>PT. Indokomas Buana Perkasa</th>
                                    <th>2009</th>
                                    <th>Wellhead</th>
                                    <th>SS316L</th>
                                    </tr> 
                                    <th>5</th>
                                    <th>Total Tunu 13</th>
                                    <th>PT. CIme Intl Service</th>
                                    <th>2009</th>
                                    <th>Wellhead Control Panel</th>
                                    <th>SS316L</th>
                                    </tr> 
                                    <th>6</th>
                                    <th>Total Tunu 12</th>
                                    <th>PT. Indokomas Buana Perkasa</th>
                                    <th>2009</th>
                                    <th>Wellhead Control Panel</th>
                                    <th>SS316L</th>
                                    </tr> 
                                    <th>7</th>
                                    <th>Gas Leak Detector</th>
                                    <th>PT. MILWAY ENG</th>
                                    <th>2009</th>
                                    <th>Wall Mounted NCH</th>
                                    <th>SS316</th>
                                    </tr> 
                                    <th>8</th>
                                    <th>VICO Surveillance</th>
                                    <th>PT. REKA ELEKTRA</th>
                                    <th>2009</th>
                                    <th>Wall Mounted NCE</th>
                                    <th>SS304</th>
                                    </tr> 
                                    <th>9</th>
                                    <th>CHEVRON</th>
                                    <th>PT. TRANSAVIA</th>
                                    <th>2008</th>
                                    <th>Wall Mounted NCE</th>
                                    <th>SS304</th>
                                    </tr>  
                                    <th>10</th>
                                    <th>VICO</th>
                                    <th>PT. WIFGASINDO</th>
                                    <th>2008</th>
                                    <th>Wall Mounted NCE</th>
                                    <th>SS304</th>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                            
                          <!-- <div class="col-lg-6 video-box">
                            <img src="assets/img/logo/led.jpg" class="img-fluid" alt="">
                            <a href="https://www.youtube.com/" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a> -->
                          </div>

      </div>
    </section><!-- End Features Section -->

    <!-- <div class="col-lg-3 col-md-6 footer-info">
      <h3>About PT. Nobi Putra Angkasa</h3>
      <p>Appointed as Siemens System Integrator in LV and ABB TCA in MV Solution</p>
      <div class="social-links mt-3">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="whatsapp"><i class="bx bxl-whatsapp"></i></a>
      </div>
    </div> -->

  </main><!-- End #main -->

  {{-- footer --}}
  @include('../footer')
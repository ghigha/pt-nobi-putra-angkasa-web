  {{-- header --}}

  @include('../header')

  <main id="main">

    <!-- ======= Our Team Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Contact Us</h2>
          <ol>
            <li><a href="home">Home</a></li>
            <li>Contact Us</li>
          </ol>
        </div>

      </div>
    </section><!-- End Our Team Section -->
    
    <iframe src="https://maps.google.com/maps?q=%20%20PT.%20Nobi%20Putra%20Angkasa&t=&z=9&ie=UTF8&iwloc=&output=embed" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

    <div class="section-title">
      <h2><strong>Jakarta Head Office</strong></h2>
      <p><a target="_blank" rel="noopener noreferrer" href="https://g.page/nobiputraangkasa?share">Jl. Pulo Buaran Raya Kav. III Blok FF1 Kawasan Industri Pulogadung Jakarta 13920 Indonesia</a></p><br> 
      <p><strong>Phone :</strong> <a href="tel:+62214602633">(62-21) 460 2633</a></p>
      <p><strong>Phone :</strong> <a href="tel:+62214602634">(62-21) 460 2634</a></p>
      <p><strong>FAX :</strong> <a href="tel:+62214602627">(62-21) 460 2627</a></p>
      <p><strong>FAX :</strong> <a href="tel:+62214602632">(62-21) 460 2632</a></p>
      <strong>Email :</strong> <a href="mailto:sales@nobi.co.id?Subject=Hello" target="_top">sales@nobi.co.id</a><br><br>
    </div>

    <div class="section-title">
      <h2><strong>Jakarta Factory</strong></h2>
      <p><a target="_blank" rel="noopener noreferrer" href="https://g.page/pt-nobi-putra-angkasa?share">Jl. Pulo Buaran Raya Kav. III Blok FF-5 Kawasan Industri Pulogadung Jakarta 13920 Indonesia</a></p><br>
      <p><strong>Phone :</strong> <a href="tel:+62214603170">(62-21) 460 3170</a></p>
      <p><strong>FAX :</strong> <a href="tel:+62214604445">(62-21) 460 4445</a></p><br>
    </div>

    <div class="section-title">
      <h2><strong>Karawang Factory</strong></h2>
      <p><a target="_blank" rel="noopener noreferrer" href="https://goo.gl/maps/5spVjzHswobbDpME9">Jl. Kosambi-Curug Dusun Mangga Besar I RT.12 RW.03</a></p><br>
      <p><strong>Phone :</strong> <a href="tel:+622678636474">(62-267) 863 6474</a></p> 
      <p><strong>Phone :</strong> <a href="tel:+622678636475">(62-267) 863 6475</a></p><br> 
    </div>

  </main><!-- End #main -->

{{-- footer --}}
@include('../footer')
  {{-- header --}}

  @include('../header')

  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>About Us</h2>
          <ol>
            <li><a href="home">Home</a></li>
            <li>About Us</li>
          </ol>
        </div>

      </div>
    </section><!-- End About Us Section -->

    <!-- ======= About Section ======= -->
    <section class="about" data-aos="fade-up">
      <div class="container">

        <div class="row">
          <div class="col-lg-6">
            <img src="assets/img/nobi-logo.svg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            <h3>Our History</h3>
            <p class="font-italic">
            PT Nobi Putra Angkasa was established on July 26, 1984 by Ir. Iman Abadi, MM with a deed of public notary Imas Fatimah, SH. notary article no. 40. PT Nobi Putra Angkasa is the first producer of cable ladders and cable trays in Indonesia, as a pioneer in the industry.
            </p>
            <p>
            The constructions of the buildings, factories, power plants, telecommunication networks, and the oil, gas, and mining facilities in Indonesia became the stimulants of the PT. Nobi Putra Angkasa establishment, development and growth.
            </p>
            <p>
            Since it was first founded, the company had been determined to keep producing the cable ladders and cable trays and other products which in line to our field of productions. Now the company had developed panel boxes or enclosures as development of product line in its business.
            </p>
            <p>
            The experience in producing of the products through the years has made PT. Nobi Putra Angkasa to further endeavor providing better services to the customer.
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Facts Section ======= -->
    <section class="facts section-bg" data-aos="fade-up">
      <div class="container">

        <div class="row counters">

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">0</span>
            <p>Clients</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">0</span>
            <p>Projects</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">0</span>
            <p>Hours Of Support</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">0</span>
            <p>Hard Workers</p>
          </div>

        </div>

      </div>
    </section><!-- End Facts Section -->

    <!-- ======= Our Skills Section ======= -->
    <section class="skills" data-aos="fade-up">
      <div class="container">

        <div class="section-title">
          <h2>Advantage</h2>
          <p>At Nobi, we adhere strictly quality procedures and control. This has earned us, among other, the internationally recognized BS EN ISO 9001 : 2000 certification and safety in the design, manufacture and use of products is foremost. On year 2009 we have upgraded the ISO certification to 2008 version.</p><br>
          <p>Our commitment to you does not end with the promise of quality products. We measure success by our ability to consistently satisfy ever changing customer preferences. We are committed to offering competitive prices and timely delivery. We are committed to yours project’s overall success.</p><br>
          <p>At Nobi, we measure our success by our accomplishments. Within a period of 25 (twenty five) years , PT. Nobi Putra Angkasa has gone through many valuable experiences and lessons which have made this company stronger.</p><br>
          <p>There are times that we work alone but, there are also things worked out in co-operation with a foreign company. The experiences so far have been realised and they could be seen from the various products made by PT. Nobi Putra Angkasa.</p><br>
          <p>On year 2009 we also start to develop Keselamatan dan Kesehatan Kerja (K3) Procedure to ensure. In running our business, we also applied work ethics to ensure.</p>
        </div>

        <!-- <div class="skills-content">

          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">HTML <i class="val">100%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">CSS <i class="val">90%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">JavaScript <i class="val">75%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">Photoshop <i class="val">55%</i></span>
            </div>
          </div>

        </div> -->

      </div>
    </section><!-- End Our Skills Section -->

        <!-- ======= technology Section ======= -->
        <section class="skills" data-aos="fade-up">
          <div class="container">
    
            <div class="section-title">
              <h2>Technology</h2>
              <p>As a manufacturing company, PT. Nobi Putra Angkasa gives more emphasis on providing the best possible services to the customers, as a problem solver for the customers, always improving our products to improve the satisfaction of customers. Therefore, we have gathered a group of highly dedicated young personnel to accomplish our aim. We invested a 3D based Mechanical Design software for helping our Engineers to develop a best engineered designs. We also continually improve our machinery with the state of the art technology available so we are always ahead in terms of production technology.</p><br>
            </div>
    
            <!-- <div class="skills-content">
    
              <div class="progress">
                <div class="progress-bar bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                  <span class="skill">HTML <i class="val">100%</i></span>
                </div>
              </div>
    
              <div class="progress">
                <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                  <span class="skill">CSS <i class="val">90%</i></span>
                </div>
              </div>
    
              <div class="progress">
                <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
                  <span class="skill">JavaScript <i class="val">75%</i></span>
                </div>
              </div>
    
              <div class="progress">
                <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">
                  <span class="skill">Photoshop <i class="val">55%</i></span>
                </div>
              </div>
    
            </div> -->
    
          </div>
        </section><!-- End technology Section -->

    <!-- ======= Tetstimonials Section ======= -->
    <section class="testimonials" data-aos="fade-up">
      <div class="container">

        <div class="section-title">
          <h2>Certification</h2>
        </div>

          <div class="section-title">
            <marquee behavior="scroll" direction="left">
              <img src="assets/img/cert/lloyds.jpg" width="150" height="100" alt="lloyds" />
              <img src="assets/img/cert/lmksmall.png" width="150" height="100" alt="lloyds" />
              <img src="assets/img/cert/pln.jpg" width="150" height="100" alt="lloyds" />
              <img src="assets/img/cert/sivacon.jpg" width="150" height="100" alt="lloyds" />
              <img src="assets/img/cert/SNI.jpg" width="150" height="100" alt="lloyds" />
              <img src="assets/img/cert/tuvrheinlandBIG.jpg" width="150" height="100" alt="lloyds" />
            </marquee>
          </div>


      </div>
    </section><!-- End Ttstimonials Section -->

  </main><!-- End #main -->

{{-- footer --}}
@include('../footer')
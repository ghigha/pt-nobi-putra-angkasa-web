  {{-- header --}}

  @include('../header')

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex justify-cntent-center align-items-center">
    <div id="heroCarousel" class="container carousel carousel-fade" data-ride="carousel">

      <!-- Slide 1 -->
      <div class="carousel-item active">
        <div class="carousel-container">
          <h2 class="animated fadeInDown">Cable Support System</h2>
          <p class="animated fadeInUp">More than 25 years of experience in design and manufacturing of cable support system.</p>
          <a href="cable-support-system" class="btn-get-started animated fadeInUp">Read More</a>
        </div>
      </div>

      <!-- Slide 2 -->
      <div class="carousel-item">
        <div class="carousel-container">
          <h2 class="animated fadeInDown">Enclosure</h2>
          <p class="animated fadeInUp">Vast capability in design and manufacturing capability of standard and customized enclosures for various usage and market.</p>
          <a href="enclosures" class="btn-get-started animated fadeInUp">Read More</a>
        </div>
      </div>

      <!-- Slide 3 -->
      <div class="carousel-item">
        <div class="carousel-container">
          <h2 class="animated fadeInDown">Electrical</h2>
          <p class="animated fadeInUp">Appointed as Siemens System Integrator in LV and ABB TCA in MV Solution.</p>
          <a href="electrical" class="btn-get-started animated fadeInUp">Read More</a>
        </div>
      </div>

      <!-- Slide 4 -->
      <div class="carousel-item">
        <div class="carousel-container">
          <h2 class="animated fadeInDown">LED Lighting</h2>
          <p class="animated fadeInUp">LED Lighting Solution Efficient, Compact, Long life, and Maintenance free.</p>
          <a href="led-lighting" class="btn-get-started animated fadeInUp">Read More</a>
        </div>
      </div>
      
    <!-- Slide 5 -->
      <div class="carousel-item">
        <div class="carousel-container">
          <h2 class="animated fadeInDown">Pole and Structure</h2>
          <p class="animated fadeInUp">Pole and Structure.</p>
          <a href="pole-structure" class="btn-get-started animated fadeInUp">Read More</a>
        </div>
      </div>

      <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon bx bx-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>

      <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon bx bx-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>

    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Services Section ======= -->
    <section class="services">
      <div class="container">

        <div class="row">
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="10">
            <div class="icon-box icon-box-pink">
              <div class="icon"><i class="bx bx-plug"></i></div>
              <h4 class="title"><a href="cable-support-system">Cable Support System</a></h4>
              <p class="description">More than 25 years of experience in design and manufacturing of cable support system.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="20">
            <div class="icon-box icon-box-cyan">
              <div class="icon"><i class="bx bx-columns"></i></div>
              <h4 class="title"><a href="enclosures">Enclosure</a></h4>
              <p class="description">Vast capability in design and manufacturing capability of standard and customized enclosures for various usage and market.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="30">
            <div class="icon-box icon-box-pink">
              <div class="icon"><i class="bx bxs-zap"></i></div>
              <h4 class="title"><a href="electrical">Electrical</a></h4>
              <p class="description">Appointed as Siemens System Integrator in LV and ABB TCA in MV Solution.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="40">
            <div class="icon-box icon-box-pink">
              <div class="icon"><i class="bx bxs-sun"></i></div>
              <h4 class="title"><a href="led-lighting">LED Lighting</a></h4>
              <p class="description">LED Lighting Solution Efficient, Compact, Long life, and Maintenance free.</p>
            </div>
          </div>
          
        <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="50">
            <div class="icon-box icon-box-pink">
              <div class="icon"><i class="bx bx-directions"></i></div>
              <h4 class="title"><a href="pole-structure">Pole and Structure</a></h4>
              <p class="description">Pole and Structure.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Why Us Section ======= -->
    <section class="why-us section-bg" data-aos="fade-up" date-aos-delay="200">
      <div class="container">

        <div class="row">
          <div class="col-lg-6 video-box">
            <img src="assets/img/CABLE SUPPORT SYSTEM/Cable-Leadder-Steel-NC-Type.jpg" class="img-fluid" alt="">
            <a href="https://www.youtube.com/" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
          </div>

          <div class="col-lg-6 d-flex flex-column justify-content-center p-5">

            <div class="icon-box">
              <div class="icon"><i class="bx bx bx-plug"></i></div>
              <h4 class="title"><a href="cable-support-system">Cable Support System</a></h4>
              <p class="description">More than 25 years of experience in design and manufacturing of cable support system.</p>
            </div>

            <!-- <div class="icon-box">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">LED Neon</a></h4>
              <p class="description">SAMPLE</p>
            </div> -->

          </div>
        </div>

        <div class="row">
          <div class="col-lg-6 video-box">
            <img src="assets/img/ENCLOSURES/NEO Freestanding Enclosure.jpg" class="img-fluid" alt="">
            <a href="https://www.youtube.com/" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
          </div>

          <div class="col-lg-6 d-flex flex-column justify-content-center p-5">

            <div class="icon-box">
              <div class="icon"><i class="bx bx-columns"></i></div>
              <h4 class="title"><a href="enclosures">Enclosures</a></h4>
              <p class="description">Vast capability in design and manufacturing capability of standard and customized enclosures for various usage and market.</p>
            </div>

            <!-- <div class="icon-box">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">Genset</a></h4>
              <p class="description">SAMPLE</p>
            </div> -->

          </div>
        </div>

        <div class="row">
          <div class="col-lg-6 video-box">
            <img src="assets/img/ELECTRICAL/sivacons8.jpg" class="img-fluid" alt="">
            <a href="https://www.youtube.com/" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
          </div>

          <div class="col-lg-6 d-flex flex-column justify-content-center p-5">

            <div class="icon-box">
              <div class="icon"><i class="bx bxs-zap"></i></div>
              <h4 class="title"><a href="electrical">Electrical</a></h4>
              <p class="description">Appointed as Siemens System Integrator in LV and ABB TCA in MV Solution.</p>
            </div>

            <!-- <div class="icon-box">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">Genset</a></h4>
              <p class="description">SAMPLE</p>
            </div> -->

          </div>
        </div>

        <div class="row">
          <div class="col-lg-6 video-box">
            <img src="assets/img/Led/nl500.jpg" class="img-fluid" alt="">
            <a href="https://www.youtube.com/" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
          </div>

          <div class="col-lg-6 d-flex flex-column justify-content-center p-5">

            <div class="icon-box">
              <div class="icon"><i class="bx bxs-sun"></i></div>
              <h4 class="title"><a href="led-lighting">LED Lighting</a></h4>
              <p class="description">LED Lighting Solution Efficient, Compact, Long life, and Maintenance free.</p>
            </div>

            <!-- <div class="icon-box">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">Genset</a></h4>
              <p class="description">SAMPLE</p>
            </div> -->

          </div>
        </div>
        
        <div class="row">
          <div class="col-lg-6 video-box">
            <img src="assets/img/pole structure/Brosur Tiang PJU (13Metter)_Page2.jpg" class="img-fluid" alt="">
            <a href="https://www.youtube.com/" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
          </div>

          <div class="col-lg-6 d-flex flex-column justify-content-center p-5">

            <div class="icon-box">
              <div class="icon"><i class="bx bx-directions"></i></div>
              <h4 class="title"><a href="pole-structure">Pole and Structure</a></h4>
              <p class="description">Pole and Structure</p>
            </div>

            <!-- <div class="icon-box">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">Genset</a></h4>
              <p class="description">SAMPLE</p>
            </div> -->

          </div>
        </div>

      </div>
    </section><!-- End Why Us Section -->

        <!-- ======= Services Section ======= -->
        <section class="services">
          <div class="container">
    
            <div class="row">
              <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                <div class="icon-box icon-box-pink">
                  <div class="icon"><i class="bx bxs-check-shield"></i></div>
                  <h4 class="title"><a href="experience">EXPERIENCES</a></h4>
                  <p class="description">Within a period of 25 (twenty five) years, PT. Nobi Putra Angkasa has gone through many valuable experiences and lessons which have made this company stronger.</p>
                </div>
              </div>
    
              <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="110">
                <div class="icon-box icon-box-cyan">
                  <div class="icon"><i class="bx bxs-factory"></i></div>
                  <h4 class="title"><a href="products">PRODUCTS</a></h4>
                  <p class="description">Within a period of 25 (twenty five) years, PT. Nobi Putra Angkasa has gone through many valuable experiences and lessons which have made this company stronger.</p>
                </div>
              </div>
    
              <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="120">
                <div class="icon-box icon-box-pink">
                  <div class="icon"><i class="bx bx-file"></i></div>
                  <h4 class="title"><a href="certification">CERTIFICATION</a></h4>
                  <p class="description">Within a period of 25 (twenty five) years, PT. Nobi Putra Angkasa has gone through many valuable experiences and lessons which have made this company stronger.</p>
                </div>
              </div>
    
              <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="130">
                <div class="icon-box icon-box-pink">
                  <div class="icon"><i class="bx bx-support"></i></div>
                  <h4 class="title"><a href="contact-us">SUPPORT</a></h4>
                  <p class="description">Within a period of 25 (twenty five) years, PT. Nobi Putra Angkasa has gone through many valuable experiences and lessons which have made this company stronger.</p>
                </div>
              </div>
    
            </div>
    
          </div>
        </section><!-- End Services Section -->

    <!-- ======= Features Section ======= -->
    <section class="features">
      <div class="container">

        <div class="section-title">
          <h2>PT. NOBI PUTRA ANGKASA</h2>
          <p>PT Nobi Putra Angkasa was established on July 26, 1984 by Ir. Iman Abadi, MM with a deed of public notary Imas Fatimah, SH. notary article no. 40. PT Nobi Putra Angkasa is the first producer of cable ladders and cable trays in Indonesia, as a pioneer in the industry.</p>
        </div>

        <div class="row" data-aos="fade-up">
          <div class="col-md-5">
            <img src="assets/img/features-1.svg" class="img-fluid" alt="">
          </div>
          <div class="col-md-7 pt-4">
            <h3>VISION</h3>
            <p class="font-italic">
              We intend to be the market leader in South East Asia Cable Tray and Enclosures Fabricator.
            </p>
          </div>
        </div>

        <div class="row" data-aos="fade-up">
          <div class="col-md-5 order-1 order-md-2">
            <img src="assets/img/features-2.svg" class="img-fluid" alt="">
          </div>
          <div class="col-md-7 pt-5 order-2 order-md-1">
            <h3>Mission</h3>
            <!-- <p class="font-italic">
              To deliver high quality products , to meet the requirements of our clients and the needs of the market.
            </p> -->
            <p>
              To deliver high quality products , to meet the requirements of our clients and the needs of the market.
            </p>
          </div>
        </div>

        <div class="row" data-aos="fade-up">
          <div class="col-md-5">
            <img src="assets/img/features-3.svg" class="img-fluid" alt="">
          </div>
          <div class="col-md-7 pt-5">
            <h3>Values</h3>
            <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
          </div>
        </div>

      </div>
    </section><!-- End Features Section -->

    <section class="skills" data-aos="fade-up">
      <div class="container">

        <div class="section-title">
          <h2>Our Client</h2>
          <marquee behavior="scroll" direction="left">
            <img src="assets/img/clients/ADARO-SERVICES.png" width="150" height="100" alt="lloyds" />
            <img src="assets/img/clients/ADARO.png" width="150" height="100" alt="lloyds" />
            <img src="assets/img/clients/BALI-TOWER.png" width="150" height="100" alt="lloyds" />
            <img src="assets/img/clients/BUMA.png" width="150" height="100" alt="lloyds" />
            <img src="assets/img/clients/DARMA-HENWA.png" width="150" height="100" alt="lloyds" />
            <img src="assets/img/clients/HUTAMA-KARYA.png" width="150" height="100" alt="lloyds" />
            <img src="assets/img/clients/IBS.png" width="150" height="100" alt="lloyds" />
            <img src="assets/img/clients/LEN-INDUSTRI.png" width="150" height="100" alt="lloyds" />
            <img src="assets/img/clients/MITRATEL.png" width="150" height="100" alt="lloyds" />
            <img src="assets/img/clients/PEMPROV-DKI.png" width="150" height="100" alt="lloyds" />
            <img src="assets/img/clients/PROTELINDO.png" width="150" height="100" alt="lloyds" />
            <img src="assets/img/clients/SINOHYDRO.png" width="150" height="100" alt="lloyds" />
            <img src="assets/img/clients/TBG.png" width="150" height="100" alt="lloyds" />
            <img src="assets/img/clients/WIKA.png" width="150" height="100" alt="lloyds" />
          </marquee>
        </div>

        <!-- <div class="skills-content">

          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">HTML <i class="val">100%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">CSS <i class="val">90%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">JavaScript <i class="val">75%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">Photoshop <i class="val">55%</i></span>
            </div>
          </div>

        </div> -->

      </div>
    </section><!-- End Our Skills Section -->

  </main><!-- End #main -->

{{-- footer --}}
  @include('../footer')
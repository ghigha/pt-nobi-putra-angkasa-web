  {{-- header --}}

  @include('../header')

  <main id="main">

    <!-- ======= Blog Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>CABLE SUPPORT SYSTEM</h2>

          <ol>
            <li><a href="home">Home</a></li>
            <li><a href="products">products</a></li>
            <li>CABLE SUPPORT SYSTEM</li>
          </ol>
        </div>

      </div>
    </section><!-- End Blog Section -->

    <!-- content -->

    <!-- ======= Features Section ======= -->
    <section class="features">
        <div class="container">
  
          <div class="section-title">
            <h2>CABLE SUPPORT SYSTEM</h2>
          </div>
  
          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/CABLE SUPPORT SYSTEM/Cable Tray Perforated.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Cable Support System/Cable Tray Perforated.pdf">Cable Tray Perforated</a></h4>
              <p class="font-italic">
                We intend to be the market leader in South East Asia Cable Tray and Enclosures Fabricator.
              </p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
              </ul> -->
            </div>
          </div>
  
          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/CABLE SUPPORT SYSTEM/Cable-Leadder-Steel-NC-Type.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Cable Support System/Cable-Leadder-Steel-NC-Type.pdf">Cable Leadder Steel NC Type</a></h4>
              <!-- <p class="font-italic">
                To deliver high quality products , to meet the requirements of our clients and the needs of the market.
              </p> -->
              <p>
                To deliver high quality products , to meet the requirements of our clients and the needs of the market.
              </p>
            </div>
          </div>
  
          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/CABLE SUPPORT SYSTEM/Cable-Leadder-Steel-NB-Type.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Cable Support System/Cable-Leadder-Steel-NB-Type.pdf">Cable Leadder Steel NB Type</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/CABLE SUPPORT SYSTEM/Cable-Leadder-Steel-SC-Type.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Cable Support System/Cable-Leadder-Steel-SC-Type.pdf">Cable Leadder Steel SC Type</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/CABLE SUPPORT SYSTEM/Cable-Leadder-Aluminum.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Cable Support System/Cable-Leadder-Aluminum.pdf">Cable Leadder Aluminum</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/CABLE SUPPORT SYSTEM/Cable-Leadder-Stainless.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Cable Support System/Cable-Leadder-Stainless.pdf">Cable Leadder Stainless</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/CABLE SUPPORT SYSTEM/Support.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Cable Support System/Support.pdf">Support</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>
  
        </div>
      </section><!-- End Features Section -->

    <!-- End content -->


  </main><!-- End #main -->

  {{-- footer --}}
  @include('../footer')
  {{-- header --}}

  @include('../header')

  <main id="main">

    <!-- ======= Blog Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>PLN Certificate</h2>

          <ol>
            <li><a href="home">Home</a></li>
            <li><a href="certification">Certification</a></li>
            <li>PLN Certificate</li>
          </ol>
        </div>

      </div>
    </section><!-- End Blog Section -->

    <!-- content -->
 
        <div style="text-align: center;"><iframe src="assets/files/sertifikasi-pln.png" width="1000px" height="1500px" frameborder="0"></iframe></div>

    <!-- End content -->

  </main><!-- End #main -->

{{-- footer --}}
@include('../footer')
<!-- ======= Footer ======= -->
<footer id="footer" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">

    <!-- <div class="footer-newsletter">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <h4>Our Newsletter</h4>
            <p>We are publishing new website</p>
          </div>
          <div class="col-lg-6">
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div>
        </div>
      </div>
    </div> -->

    <!-- <div class="row">
      <div class=”col-md-6 col-md-offset-3”>
      <img src="assets/img/cert/lloyds.jpg" class="center">
      <img src="assets/img/cert/lmksmall.png" class="center">
      <img src="assets/img/cert/siemens.jpg" class="center">
      <img src="assets/img/cert/sni.jpg" class="center">
      <img src="assets/img/cert/tuv.png" class="center">
    </div>
  </div> -->

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <!-- <div class="col-lg-3 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div> -->

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="cable-support-system">Cable Support System</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="enclosures">Enclosures</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="electrical">Electrical</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="led-lighting">LED Lighting</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="speed-radar">Speed radar</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="pole-structure">Pole and Structure</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4> </h4>
            <p>
              <h4><strong>Jakarta Head Office</h4>
              Jl. Pulo Buaran Raya Kav. III  <br>
              Blok FF1 Kawasan Industri Pulogadung<br>
              Jakarta 13920 Indonesia </strong><br><br>
            <p><strong>Phone :</strong> <a href="tel:+62214602633">(62-21) 460 2633</a></p>
            <p><strong>Phone :</strong> <a href="tel:+62214602634">(62-21) 460 2634</a></p>
            <p><strong>FAX :</strong> <a href="tel:+62214602627">(62-21) 460 2627</a></p>
            <p><strong>FAX :</strong> <a href="tel:+62214602632">(62-21) 460 2632</a></p>
            <strong>Email :</strong> <a href="mailto:sales@nobi.co.id?Subject=Hello" target="_top">sales@nobi.co.id</a><br><br>
            </p>
          </div>
          <div class="col-lg-3 col-md-6 footer-contact">
            <h4> </h4>
            <p>
              <h4><strong>Jakarta Factory</h4>
              Jl. Pulo Buaran Raya Kav. III  <br>
              Blok FF-5 Kawasan Industri Pulogadung<br>
              Jakarta 13920 Indonesia </strong><br><br>
            <p><strong>Phone :</strong> <a href="tel:+62214603170">(62-21) 460 3170</a></p>
            <p><strong>FAX :</strong> <a href="tel:+62214604445">(62-21) 460 4445</a></p><br>
            </p>
          </div>
            <div class="col-lg-3 col-md-6 footer-contact">
              <h4> </h4>
              <p>
              <h4><strong>Karawang Factory</h4>
              Jl. Kosambi-Curug  <br>
              Dusun Mangga Besar I RT.12 RW.03</strong><br><br>
            <p><strong>Phone :</strong> <a href="tel:+622678636474">(62-267) 863 6474</a></p> 
            <p><strong>Phone :</strong> <a href="tel:+622678636475">(62-267) 863 6475</a></p><br> 
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-info">
            <h3>About PT. Nobi Putra Angkasa</h3>
            <p>Appointed as Siemens System Integrator in LV and ABB TCA in MV Solution</p>
            <div class="social-links mt-3">
              <a href="https://www.facebook.com/ptnobiputraangkasa" class="facebook"><i class="bx bxl-facebook"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        Copyright &copy; 2020 <strong><span>PT. NOBI PUTRA ANGKASA</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/free-bootstrap-template-corporate-moderna/ -->
        Designed by <a href="https://nobi.co.id/">Team IT NOBI</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <a class='fixed-whatsapp' href='https://api.whatsapp.com/send?phone=+628129234474' rel='nofollow noopener' target='_blank' title='Whatsapp'>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>
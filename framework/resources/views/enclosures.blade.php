  {{-- header --}}

  @include('../header')

  <main id="main">

    <!-- ======= Blog Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>ENCLOSURES</h2>

          <ol>
            <li><a href="home">Home</a></li>
            <li><a href="products">Products</a></li>
            <li>ENCLOSURES</li>
          </ol>
        </div>

      </div>
    </section><!-- End Blog Section -->

    <!-- content -->

    <!-- ======= Features Section ======= -->
    <section class="features">
        <div class="container">
  
          <div class="section-title">
            <h2>ENCLOSURES</h2>
          </div>
  
          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/ENCLOSURES/NEO Freestanding Enclosure.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Enclosures/NEO Freestanding Enclosure.pdf">NEO Freestanding Enclosure</a></h4>
              <p class="font-italic">
                We intend to be the market leader in South East Asia Cable Tray and Enclosures Fabricator.
              </p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
              </ul> -->
            </div>
          </div>
  
          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/ENCLOSURES/FSH Stainless Steel Enclosure.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Enclosures/FSH Stainless Steel Enclosure.pdf">FSH Stainless Steel Enclosure</a></h4>
              <!-- <p class="font-italic">
                To deliver high quality products , to meet the requirements of our clients and the needs of the market.
              </p> -->
              <p>
                To deliver high quality products , to meet the requirements of our clients and the needs of the market.
              </p>
            </div>
          </div>
  
          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/ENCLOSURES/FSH Stainless Steel Enclosure.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Enclosures/">FSE Stainless Steel Enclosure</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/ENCLOSURES/KDO Enclosure.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Enclosures/KDO Enclosure.pdf">KDO Enclosure</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/ENCLOSURES/KDE Enclosure.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Enclosures/KDE Enclosure.pdf">KDE Enclosure</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/ENCLOSURES/KDH Enclosure.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Enclosures/KDH Enclosure.pdf">KDH Enclosure</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/ENCLOSURES/KDS Enclosure.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Enclosures/KDS Enclosure.pdf">KDS Enclosure</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/ENCLOSURES/SS 3000 Series Enclosure.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Enclosures/SS 3000 Series Enclosure.pdf">SS 3000 Series Enclosure</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/ENCLOSURES/NCH Stainless Steel Enclosure.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Enclosures/NCH Stainless Steel Enclosure.pdf">NCH Stainless Steel Enclosure</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/ENCLOSURES/NCH Steel Enclosure.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Enclosures/NCH Steel Enclosure.pdf">NCE-Enclosure</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/ENCLOSURES/NKL Enclosure.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Enclosures/NKL Enclosure.pdf">NKL-Enclosure</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/ENCLOSURES/NEO Pole Mounting Enclosure.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Enclosures/NEO Pole Mounting Enclosure.pdf">NEO Pole Mounting Enclosure</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/ENCLOSURES/Enclosure Accessories.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="assets/files/Enclosures/Enclosure Accessories.pdf">Enclosure Accessories</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
              <!-- <ul>
                <li><i class="icofont-check"></i> SAMPLE 1</li>
                <li><i class="icofont-check"></i> SAMPLE 2</li>
                <li><i class="icofont-check"></i> SAMPLE 3</li>
              </ul> -->
            </div>
          </div>
  
        </div>
      </section><!-- End Features Section -->

    <!-- End content -->


  </main><!-- End #main -->

 {{-- footer --}}
 @include('../footer')
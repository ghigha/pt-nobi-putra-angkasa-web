  {{-- header --}}

  @include('../header')

  <main id="main">

    <!-- ======= Blog Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>CERTIFICATION</h2>

          <ol>
            <li><a href="home">Home</a></li>
            <li>CERTIFICATION</li>
          </ol>
        </div>

      </div>
    </section><!-- End Blog Section -->

    <!-- content -->

    <!-- ======= Features Section ======= -->
    <section class="features">
        <div class="container">
  
          <div class="section-title">
            <h2>CERTIFICATION</h2>
            <p></p>
          </div>
  
          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/cert/tuvrheinlandBIG.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-4">
              <h4 class="title"><a href="https://www.certipedia.com/quality_marks/0910089376?locale=en&certificate_number=01+100+89376%2F02">Certipedia by TÜV Rheinland</a></h4>
              <p class="font-italic">
                We intend to be the market leader in South East Asia Cable Tray and Enclosures Fabricator.
              </p>
            </div>
          </div>
  
          <div class="row" data-aos="fade-up">
            <div class="col-md-5 order-1 order-md-2">
              <img src="assets/img/cert/lloyds.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5 order-2 order-md-1">
              <h4 class="title"><a href="lloyds1">LLoyd's Certificate LRQA Cable Tray</a></h4>
              <!-- <p class="font-italic">
                To deliver high quality products , to meet the requirements of our clients and the needs of the market.
              </p> -->
              <p>
                To deliver high quality products , to meet the requirements of our clients and the needs of the market.
              </p>
            </div>
          </div>
  
          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/cert/lloyds.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="lloyds2">LLoyd's Certificate Speciment Test Of Cable Tray LRQA</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5 order-1 order-md-2">
              <img src="assets/img/cert/pln.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5 order-2 order-md-1">
              <h4 class="title"><a href="pln">PLN Certificate</a></h4>
              <!-- <p class="font-italic">
                To deliver high quality products , to meet the requirements of our clients and the needs of the market.
              </p> -->
              <p>
                To deliver high quality products , to meet the requirements of our clients and the needs of the market.
              </p>
            </div>
          </div>
  
          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/cert/tuvrheinlandBIG.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="tuv1">TUV Certificate</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
            </div>
          </div>

          <div class="row" data-aos="fade-up">
            <div class="col-md-5 order-1 order-md-2">
              <img src="assets/img/cert/SNI.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5 order-2 order-md-1">
              <h4 class="title"><a href="sni">SNI Certificate</a></h4>
              <!-- <p class="font-italic">
                To deliver high quality products , to meet the requirements of our clients and the needs of the market.
              </p> -->
              <p>
                To deliver high quality products , to meet the requirements of our clients and the needs of the market.
              </p>
            </div>
          </div>
  
          <div class="row" data-aos="fade-up">
            <div class="col-md-5">
              <img src="assets/img/cert/sivacon.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-md-7 pt-5">
              <h4 class="title"><a href="sivacon">Sivacon Technology Partner</a></h4>
              <p>In order to achieve this, we always thrive to satisfy our Customers, Employees and Shareholder to the best possible.</p>
            </div>
          </div>
  
        </div>
      </section><!-- End Features Section -->

    <!-- End content -->


  </main><!-- End #main -->

{{-- footer --}}
@include('../footer')
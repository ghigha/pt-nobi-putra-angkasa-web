  {{-- header --}}

  @include('../header')

  <main id="main">

    <!-- ======= Blog Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Career</h2>

          <ol>
            <li><a href="index">Home</a></li>
            <li>Career</li>
          </ol>
        </div>

      </div>
    </section><!-- End Blog Section -->

    <!-- content -->

    <div class="section-title">
      <h2>Working at PT. NOBI PUTRA ANGKASA</h2>
      <p>We are creating a great future for our company and our country. With the various skills and abilities, everyone can be a part of it. Take the opportunities and join the contribution to buid ahead together with </p>
      <p><strong>PT. NOBI PUTRA ANGKASA.</strong></p><br>
      <p>Explore career opportunities below.</p><br>
      <p><strong>Sorry, there are no positions available at this time</strong></p><br>
    </div>

    <!-- End content -->


  </main><!-- End #main -->

{{-- footer --}}
@include('../footer')